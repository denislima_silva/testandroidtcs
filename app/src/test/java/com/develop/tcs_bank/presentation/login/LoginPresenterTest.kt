package com.develop.tcs_bank.presentation.login

import android.app.Application
import com.develop.tcs_bank.presentation.main.TcsApplication
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class LoginPresenterTest {


    private lateinit var loginPresenter: LoginContract.Presenter

    @Mock
    private lateinit var loginView: LoginView

    @Before
    fun setUp(){


        MockitoAnnotations.initMocks(this)
        loginPresenter = LoginPresenter(loginView)

    }

    @Test
    fun processLogin_NullString(){

        loginPresenter.processLogin("","")
        verify(loginView, never())

    }
}